package com.puravida.groogle.bigquery

import com.puravida.groogle.Groogle

import java.util.function.Consumer

interface BigQueryService extends Groogle.GroogleService{

    List<Map> rows( String sql)

    int eachRow( String sql, Consumer<Map> consumer)

    void execute( String sql)

}