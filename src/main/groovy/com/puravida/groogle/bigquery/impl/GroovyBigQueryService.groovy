package com.puravida.groogle.bigquery.impl

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.bigquery.Bigquery
import com.google.cloud.bigquery.BigQuery
import com.google.cloud.bigquery.BigQueryOptions
import com.google.cloud.bigquery.Field
import com.google.cloud.bigquery.FieldValue
import com.google.cloud.bigquery.FieldValueList
import com.google.cloud.bigquery.Job
import com.google.cloud.bigquery.JobId
import com.google.cloud.bigquery.JobInfo
import com.google.cloud.bigquery.QueryJobConfiguration
import com.google.cloud.bigquery.QueryResponse
import com.google.cloud.bigquery.TableResult
import com.puravida.groogle.bigquery.BigQueryService
import com.puravida.groogle.impl.InternalService
import groovy.transform.CompileStatic

import java.util.function.Consumer

@CompileStatic
class GroovyBigQueryService implements InternalService, BigQueryService{

    BigQuery service

    @Override
    void configure(JsonFactory jsonFactory, HttpTransport httpTransport, Credential credential, String applicationName) {
        this.service = BigQueryOptions.defaultInstance.service
    }

    @Override
    List<Map> rows(String sql) {
        List<Map> ret =[]
        eachRow(sql, { Map row ->
            ret.add(row)
        } as Consumer)
        ret
    }

    @Override
    int eachRow(String sql, Consumer<Map> consumer) {
        QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(sql).setUseLegacySql(false).build()

        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = service.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build())

        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists")
        }

        if (queryJob.status.error != null) {
            throw new RuntimeException("$queryJob.status.error");
        }

        TableResult tableResult = queryJob.getQueryResults()
        int count=0
        for (FieldValueList row : tableResult.iterateAll()) {
            Map item = [:]
            tableResult.schema.fields.each { Field f->
                item[f.name] = row.get(f.name).value
            }
            consumer.accept(item)
            count++
        }
        count
    }

    @Override
    void execute(String sql) {

    }
}
